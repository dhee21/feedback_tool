<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ratings
 *
 * @ORM\Table(name="ratings")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ratingsRepository")
 */
class ratings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="card_id", type="integer")
     * @ORM\OneToOne(targetEntity="cards",inversedBy="id")
     * @ORM\JoinColumn(name="card_id" , referencedColumnName="id")
     */
    private $cardId;

    /**
     * @var int
     *
     * @ORM\Column(name="fp_id", type="integer", nullable=true)
     */
    private $fpId;

    /**
     * @var string
     *
     * @ORM\Column(name="val", type="string", length=255)
     * @ORM\ManyToOne(targetEntity="feedback_providers",inversedBy="id")
     * @ORM\JoinColumn(name="fp_id" , referencedColumnName="id")
     */
    private $val;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardId
     *
     * @param integer $cardId
     *
     * @return ratings
     */
    public function setCardId($cardId)
    {
        $this->cardId = $cardId;

        return $this;
    }

    /**
     * Get cardId
     *
     * @return int
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * Set fpId
     *
     * @param integer $fpId
     *
     * @return ratings
     */
    public function setFpId($fpId)
    {
        $this->fpId = $fpId;

        return $this;
    }

    /**
     * Get fpId
     *
     * @return int
     */
    public function getFpId()
    {
        return $this->fpId;
    }

    /**
     * Set val
     *
     * @param string $val
     *
     * @return ratings
     */
    public function setVal($val)
    {
        $this->val = $val;

        return $this;
    }

    /**
     * Get val
     *
     * @return string
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ratings
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ratings
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
