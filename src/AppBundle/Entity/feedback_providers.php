<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * feedback_providers
 *
 * @ORM\Table(name="feedback_providers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\feedback_providersRepository")
 */
class feedback_providers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToOne(targetEntity="rating", mappedBy="cardId")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cookie_id",type="string", length=255, nullable=true)
     */
    private $cookieId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cookieId
     *
     * @param integer $cookieId
     *
     * @return feedback_providers
     */
    public function setCookieId($cookieId)
    {
        $this->cookieId = $cookieId;

        return $this;
    }

    /**
     * Get cookieId
     *
     * @return int
     */
    public function getCookieId()
    {
        return $this->cookieId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return feedback_providers
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return feedback_providers
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
