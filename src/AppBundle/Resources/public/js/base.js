$(document).ready(function(){
	$('#flash_message').hide();//to initially hide flash message in modal of login 


	setTimeout(function() {
		$('.flash').fadeOut('fast');
	}, 5000); 

	$('#sign_up').click(function(){
		var email=$('#reg_username').val();
		var pass=$('#reg_password').val();
		var pass_confirm=$('#reg_password_confirm').val();
		//console.log(email+' '+pass+' '+pass_confirm);
		var post_data={'reg_username':email,'reg_password':pass,'reg_password_confirm':pass_confirm};
		$.ajax({
			url:  Settings.base_url+"/save_details ",
			data:post_data,
			type: "POST",

			success: function(data) {

				if(data=='successfully_registered')
				{
					$('#modal-register').modal('hide');                	
					$('#modal-register-login').modal('show');
					$('#flash_message').html('Successfully registered ,Now log in please');
					$('#flash_message').show();

					setTimeout(function() {
						$('.flash').fadeOut('fast');
					}, 2000); 

				}
				else
				{
					location.reload();
				}

			}
		});
	});

	$('#log_in').click(function(){
		var email=$('#lg_username').val();
		var pass=$('#lg_password').val();
		//console.log(email+' '+pass);
		var post_data={'lg_username':email,'lg_password':pass};

		$.ajax({
			url:  Settings.base_url+"/afterlogin",
			data:post_data,
			type: "POST",

			success: function(data) {
				if(data=='wrong_account' || data=='wrong_password')
				{
					location.reload();
				}
				else
				{
					var address=data;
					window.location=Settings.base_url+'/'+address;
				}

			}
		});

	});

  $('#make_cards').on('click', '.cut', function(el){
  	
    ($(this).parent()).remove();
})

});