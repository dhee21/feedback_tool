var cards_array;
var present_card=0;
var number_of_cards;
var feedback_provider_id;

$(document).ready(function(){
	var feedback_page=$('.enclosure').attr('id');
	
	var client=$('.enclosure').attr('client');

	$.ajax({//to check number of cards submitted by "this" user of given feedback page
		url:'/cards_submitted/'+client,
		type:"POST",
		success:function(data)
		{
			jsonData=$.parseJSON(data);//returns numer of cards submited
			
			present_card=jsonData['submitted_cards'];
			feedback_provider_id=jsonData['fp_id'];
			console.log(feedback_provider_id);
		}
					
	});

	
	$.ajax({
		url:'/cards/'+feedback_page,
		type:"POST",
		success:function(data)
		{
			//console.log(data);
			jsonData=$.parseJSON(data);
			
			cards_array=jsonData;
			number_of_cards=cards_array.length;
			if(present_card<number_of_cards)
			{
			   $('#feedback_image').attr('src',cards_array[present_card].src);
			   $('#feedback_text').html(cards_array[present_card].text);
			}
			else
			{
					alert('thank you you have already given your feedback');
			        window.location='/';	
			}
			
			

		}
					
	});


	$('.response_button').click(function()
	{
		card_id_val=cards_array[present_card].id;
		saveRating(feedback_provider_id,card_id_val,$(this).attr('value'));


		if(present_card+1<number_of_cards)
		{
			present_card++;
			$('#feedback_image').attr('src',cards_array[present_card].src);
			$('#feedback_text').html(cards_array[present_card].text);
			

		}
		else
		{

				alert('thank you');
				window.location='/';
		}
	});

	

	function saveRating(fp_id,card_id,val)
	{
		var post_data={
			'fp_id':fp_id,
			'card_id':card_id,
			'val':val
		};

		$.ajax({
		url:'/ratings',
		data:post_data,
		type:"POST",			
	          });
	}

});