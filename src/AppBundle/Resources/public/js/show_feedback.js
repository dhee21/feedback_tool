var cards_array;
var present_card=0;
var number_of_cards;
$(document).ready(function(){
	var feedback_page=$('.enclosure').attr('id');	
	$.ajax({
		url:Settings.base_url+'/cards/'+feedback_page,
		type:"POST",
		success:function(data)
		{
			//console.log(data);
			jsonData=$.parseJSON(data);
			
			cards_array=jsonData;
			

			number_of_cards=cards_array.length;
			if(present_card<number_of_cards)
			{
			   $('#feedback_image').attr('src',cards_array[present_card].src);
			   $('#feedback_text').html(cards_array[present_card].text);
			}
			else
			{
					alert('thank you you have already given your feedback');
			}
			
			

		}
					
	});

	$('#yes_button').click(function()
	{		
		if(present_card+1<number_of_cards)
		{
			present_card++;
			$('#feedback_image').attr('src',cards_array[present_card].src);
			$('#feedback_text').html(cards_array[present_card].text);
			

		}
		else
		{
				alert('thank you');
				
		}
	});

	$('#no_button').click(function()
	{
		card_id_val=cards_array[present_card].id;
		saveRating(feedback_provider_id,card_id_val,'no');

		if(present_card+1<number_of_cards)
		{
			present_card++;
			$('#feedback_image').attr('src',cards_array[present_card].src);
			$('#feedback_text').html(cards_array[present_card].text);
			
		}
		else
		{ 
			alert('thank you');			
		}
	});

});