<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\users;
use AppBundle\Entity\cards;
use AppBundle\Entity\feedback_page;
use AppBundle\Entity\feedback_providers;
use AppBundle\Entity\ratings;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;

class feedbackController extends Controller
{


	/**
	*@Route("/",name="root")
	*/
	public function rootAction(Request $request)  
	{
		$session=$request->getSession();

		if($session->has('user'))
		{
			$user_arr=$session->get('user');
			return $this->redirect('/account/'.$user_arr['user_id']);
		}
		else
		{
			if($session->has('before_data'))
			{
			  $session->remove('before_data');
			}
			if($session->has('type'))
			{
				$session->remove('type');
			}
			return $this->render('feedback/first_page.html.twig');
		}
	}

	
	/**
	*@Route("/afterlogin" ,name="afterlogin")
	**/
	public function afterloginAction(Request $request)//when u press login this page comes
	{

		
		$username=$request->request->get('lg_username');
		$pass=$request->request->get('lg_password');

		$repo=$this->getDoctrine()->getRepository('AppBundle:users');
		$user_db=$repo->findOneByEmail($username);

		if(!$user_db)
		{
			$this->addFlash(
				'error',
				'No such account'
				);
			return new Response('wrong_account');
		}

		$pass_db=$user_db->getPassword();
		$salt_db=$user_db->getSlug();
		$user_id=$user_db->getId();

		$check_pass=$password=md5($salt_db.$pass);

		if($pass_db!=$check_pass)
		{
			$this->addFlash(
				'error',
				'Wrong password'
				);
			return new Response('wrong_password');
		}


		$session=$request->getSession();
		$session->set('user',array('user_id' => $user_id));
		if($session->has('before_data'))
		{
			$type=$session->get('type');
			$address='makeform/'.$user_id.'/'.$type;
			return new Response($address);
			
		}
		$address='account/'.$user_id;
		return new Response($address);
		

       
	}

	/**
	*@Route("/account/{user_id}",name="account_page")
	*/
	public function accountAction(Request $request,$user_id)  
	{
		$session=$request->getSession();
		if($session->has('user') && $session->get('user')['user_id']==$user_id)
		{
			return $this->render('feedback/user_page.html.twig',array('user_id'=>$user_id));
		}
		return $this->redirectToRoute('root');

		
	}

	/**
	*@Route("/account/oldforms/{user_id}",name="old_forms_page")
	*/
	public function oldFormAction(Request $request,$user_id)  
	{
		$session=$request->getSession();
		if(!$session->has('user') || $session->get('user')['user_id']!=$user_id)
		{
			return $this->redirectToRoute('root');
		}
		$repo=$this->getDoctrine()->getRepository('AppBundle:feedback_page');
		$query=$repo->createQueryBuilder('p')
		->where('p.userId = :user_id')
		->setParameter('user_id',$user_id)
		->getquery();

		$feedback_forms=$query->getResult();

		return $this->render('feedback/form_page.html.twig',array('forms'=>$feedback_forms,'user'=>$user_id));
	}



	

	/**
	*@Route("save_details",name="save_details")
	*/
	public function saveDetailsAction(Request $request)  // to save registration details to database
	{
        $email=$request->request->get('reg_username');
        $password=$request->request->get('reg_password');
        $password_confirm=$request->request->get('reg_password_confirm');
        //$first_name=$request->request->get('reg_firstname');
        //$last_name=$request->request->get('reg_lastname');
        //$mobile_no=$request->request->get('reg_mobile_no');
        //$address=$request->request->get('reg_address');
        //$company_name=$request->request->get('reg_company_name');
        $created_at=$now=new\DateTime('now');



        if($email=='' || $password=='' || $password_confirm=='')
        {
        	$this->addFlash(
        			'error',
        			'fill all starred details'
        		);
        	return new Response('incomplete_error');
        }

        if($password!=$password_confirm){
        	$this->addFlash(
        			'error',
        			'both password does not match'
        		);
        	return new Response('password_error');
        }

        $salt=feedbackController::rand_string(10);
        $password=md5($salt.$password); 



        $user= new users;
        $user->setEmail($email);
        $user->setPassword($password);
        //$user->setFirstName($first_name);
       // $user->setLastName($last_name);
        //$user->setMobile($mobile_no);
        //$user->setAddress($address);
        //$user->setCompanyName($company_name);
        $user->setSlug($salt);
        $user->setCreatedAt($created_at);
        $user->setUpdatedAt($created_at);
    
       $em=$this->getDoctrine()->getManager();
       $errors=$this->get('validator')->validate($user);
       if(count($errors)>0)
       {
       	  $errorString=(string) $errors;
       		$this->addFlash(
        			'error',
        			$errors
        		);
        	return new Response('validation_error');
       }

       if($this->getDoctrine()->getRepository('AppBundle:users')->findOneByEmail($email) )
       {	
       		
       		$this->addFlash(
        			'error',
        			'you already have an account'
        		);
        	return new Response('already_account');
       }

       $em->persist($user);
       $em->flush();

       		
        	return new Response('successfully_registered');
	}


	

	/**
	*@Route("/make_form_login/{type}",name="make_form_before_login")
	*/
	public function makeFormLoginAction(Request $request,$type)//to go to page called make form before login
	{
		$session=$request->getSession();
		//$session->invalidate();
		
		$form_name='';$cards_array=[];
		if($session->has('type') && $session->get('type')==$type)
		{
			if($session->has('before_data'))
			{
				$before_data=$session->get('before_data');
				$cards_array=$before_data['cards_array'];
			}
			if($session->has('form_name'))
			{
				$form_name=$before_data['form_name'];
			}
		}
		else
		{	
			 $session->set('type',$type);
			 
		}
		return $this->render('feedback/make_form_before_login.html.twig',array('form_name'=>$form_name,'cards_array'=>$cards_array,'type'=>$type));
	}

	/**
	*@Route("/makeform/{user_id}/{type}",name="make_form_page")
	*/
	public function makeFormAction(Request $request,$user_id,$type)//to go to page called make form where we can make different
	{
		$session=$request->getSession();
		if(!$session->has('user') || $session->get('user')['user_id']!=$user_id)
		{
			return $this->redirectToRoute('root');
		}
		$form_name='';$cards_array=[];
		if($session->has('before_data'))
		{
			$before_data=$session->get('before_data');
			$form_name=$before_data['form_name'];
			$cards_array=$before_data['cards_array'];
			$session->remove('before_data');

		}
		else
		{
			$session->set('type',$type);
		}
		return $this->render('feedback/make_form.html.twig',array('user'=>$user_id,'form_name'=>$form_name,'cards_array'=>$cards_array));
	}

	

	/**
	*@Route("/get_card_form",name="get_card_form")
	*/
	public function getCardFormAction(Request $request) //to get the form for making a card
	{
		$form=$this->createFormBuilder()
		->add('text_val',TextareaType::class,array('attr'=>array('class'=>'form-control','style'=>'margin-bottom:15px')) )
		->add('image_reference',TextType::class,array('attr'=>array('class'=>'form-control','style'=>'margin-bottom:15px')))
		->getForm();

		 $form->handleRequest($request);

		return $this->render('feedback/card_form.html.twig',array('form'=>$form->createView()));
	}

	/**
	*@Route("/get_card",name="get_card")
	*/
	public function getCardAction(Request $request)//when a card form filled corresponding card is made through this
	{	 
			$image=$_POST['image'];
			$text=$_POST['text'];

			return $this->render('feedback/card.html.twig',array('image'=>$image,'text'=>$text));
	}
 	
 	/**
	*@Route("/save_cards",name="save_cards")
	*/
	public function saveCardsAction(Request $request)//save cards of a user form when save card button pressed
	{	 
	  
	  $user=$_POST['user_number'];
	  $name=$_POST['name_of_form'];
	  $array_of_cards=json_decode($_POST['cards']);
	  $session=$request->getSession();
	  $type=$session->get('type');

	  $feedback= new feedback_page;
	  $feedback->setUserId($user);
	  $feedback->setCreatedAt(new\DateTime('now'));
	  $feedback->setName($name);
	  $feedback->setType($type);
	  $feedback->setUpdatedAt(new\DateTime('now'));
	  $em=$this->getDoctrine()->getManager();
	  $em->persist($feedback);
	  $em->flush();
	  $id=$feedback->getId();

	  for($i=0;$i<sizeof($array_of_cards);$i++)
	  {
	  	$card=new cards;
	  	$card->setFeedbackId($feedback->getId());
	  	$card->setText($array_of_cards[$i][1]);
	  	$card->setImage($array_of_cards[$i][0]);
	  	$card->setCreatedAt(new\DateTime('now'));
	  	$card->setUpdatedAt(new\DateTime('now'));
	  	//$card->setFeedbackId($feedback);
	  	$em->persist($card);
	  	 $em->flush();
	  
	  }

	  $this->addFlash(
	  	'notice',
	  	'successfully saved'
	  	);
	  $session->remove('type');
	  
	  return new Response($id);
			
	}

	/**
	*@Route("/share/{user}/{feedback_id}",name="share_page")
	*/
	public function sharePageAction(Request $request,$user,$feedback_id)//when we come on the share
	{
		return $this->render('feedback/share_page.html.twig',array('user'=>$user,'feedback_id'=>$feedback_id));
	}


  	/**
	*@Route("/feedback/{feedback_id}",name="feedback_page")
	*/
	public function feedbackAction(Request $request,$feedback_id)//when we come on the feedback page
	{
			$cookie=$request->cookies;
			$response=new Response;
			$client_id=$request->getClientIp().'-'.$feedback_id;

			$page=$this->getDoctrine()->getRepository('AppBundle:feedback_page')->findOneBy(array('id'=>$feedback_id));
			$page_type=$page->getType();

			if(!$cookie->has('client_id'))//this is the user id of the browser client
			{
				
				$response=$this->render('feedback/give_feedback.html.twig',array('feedback_id'=>$feedback_id,'client'=>$client_id,'type'=>$page_type));
				$response->headers->setcookie(new Cookie('client_id',$client_id,time()+(3600 * 4)));
				
			}
			else
			{
				$response=$this->render('feedback/give_feedback.html.twig',array('feedback_id'=>$feedback_id,'client'=>$client_id,'type'=>$page_type));
			}
			
			if(count($this->getDoctrine()->getRepository('AppBundle:feedback_providers')->findBy(array('cookieId'=>$client_id)))==0)
			{
				$em=$this->getDoctrine()->getManager();
				$feedback_provider=new feedback_providers;
				$feedback_provider->setCookieId($client_id);
				$feedback_provider->setCreatedAt(new\DateTime('now'));
				$feedback_provider->setUpdatedAt(new\DateTime('now'));
				$em->persist($feedback_provider);
				$em->flush();
			}
			
			
			return $response;		
	}


	/**
	*@Route("/show_feedback/{user_id}/{feedback_id}",name="show_feedback_page")
	*/
	public function showFeedbackAction(Request $request,$user_id,$feedback_id)//when we come on the feedback page
	{
				$session=$request->getSession();
				if(!$session->has('user') || $session->get('user')['user_id']!=$user_id)
				{
					return $this->redirectToRoute('root');
				}
				$cards_array=feedbackController::cardsFromDatabaseAction($feedback_id , 0);			
				return $this->render('feedback/show_feedback.html.twig',array('user'=>$user_id,'feedback_id'=>$feedback_id,'cards_array'=>$cards_array));		
	}




	/**
	*@Route("/cards/{feedback_id}",name="fetch_cards_from_database")
	*/
	public function cardsFromDatabaseAction($feedback_id, $flag=1)
	{
			$cards=$this->getDoctrine()->getRepository('AppBundle:cards')->findBy(array('feedbackId'=>$feedback_id));
			$cards_to_send=[];
			foreach($cards as $value)
			{
				$card=new cards;
				$card=$value;
				$id=$card->getId();
				$src=$card->getImage();
				$text=$card->getText();
					if($flag==0)
					{
						$rating_array=$this->getDoctrine()->getRepository('AppBundle:ratings')->findRatingArray($id);//values of card_id
						
						$percentage_array=$this->getDoctrine()->getRepository('AppBundle:ratings')->findRatingPercentage($rating_array);//%es of yes/no

						$arr=array('id'=>$id,'src'=>$src,'text'=>$text,'rating_array'=>$percentage_array);
					}
				
					if($flag!=0)
					{
						$arr=array('id'=>$id,'src'=>$src,'text'=>$text);
					}
				
				array_push($cards_to_send,$arr );
			}

			
			if($flag==0)
			{
				return $cards_to_send;
			}


			return new Response(json_encode($cards_to_send));
	
			
	}

	/**
	*@Route("/cards_submitted/{client}",name="cards_submitted")
	*/
	public function cardsSubmittedAction($client)
	{
		
		$fp_id=$this->getDoctrine()->getRepository('AppBundle:feedback_providers')->findOneBy(array('cookieId'=>$client))->getId();
		$cards_submitted_num=count($this->getDoctrine()->getRepository('AppBundle:ratings')->findBy(array('fpId'=>$fp_id)));

		return new Response(json_encode(array('submitted_cards'=>$cards_submitted_num,'fp_id'=>$fp_id)));
	}
 

    /**
	*@Route("/ratings",name="save_ratings")
	*/
	public function saveRatingAction()
	{
		$fp_id=$_POST['fp_id'];
		$card_id=$_POST['card_id'];
		$val=$_POST['val'];
		$em=$this->getDoctrine()->getManager();
		$rating=new ratings;
		$rating->setFpId($fp_id);
		$rating->setCardId($card_id);
		$rating->setVal($val);
		$rating->setCreatedAt(new\DateTime('now'));
		$rating->setUpdatedAt(new\DateTime('now'));
		$em->persist($rating);

		$em->flush();
		return new Response(json_encode($rating->getVal()));


	}

	/**
	*@Route("/save_session_data/{from_page}",name="save_session_data")
	*/
	public function saveSessionDataAction(Request $request,$from_page)
	{
			if($from_page=='before_login')
			{
				$form_name=$_POST['form_name'];
				$cards_array=$_POST['cards_array'];
				$before_data=array('form_name' =>$form_name ,'cards_array'=>$cards_array);
				$request->getSession()->set('before_data', $before_data);
				
				return new Response('successff');
			}
	}

	/**
	*@Route("/logout",name="logoutAction")
	*/
	public function logoutAction(Request $request)  
	{
		$request->getSession()->invalidate();
		return $this->redirectToRoute('root');		
	}
   	
   	/**
   	*@Route("/upload",name="upload")
   	*/
   	public function uploadAction(Request $request)
   	{
   		$file=$request->files->get('files');


   		$x=$this->get('app.images_uploader');

   		$filename=$x->upload($file[0]);
   		$base_url=$this->container->get('router')->getContext()->getBaseUrl();
   		$host=$request->getHttpHost();
   		
   		return new Response('http://'.$host.$base_url.'/uploads/images/'.$filename);

   	}
    

    public function rand_string( $length ) {//for hashing password
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
    $size = strlen( $chars );
    $str='';
    for( $i = 0; $i < $length; $i++ ) {
        $str .= $chars[ rand( 0, $size - 1 ) ];
    }
    return $str;
        }  


    


   }